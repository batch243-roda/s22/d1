console.log("Siesta Time");

// Array methods

// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array elements

const fruits = ["Apple", "Orange", "kiwi", "Dragon fruit"];
console.log(fruits);

// push()
/**
 * -Adds an element in the end of an array and returns the array's length.
 * Syntax:
 *    arrayName.push(elementsToBeAdded);
 */

console.log("Current Array fruits[]: ");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log("Mutated array from push() method: ");
console.log(fruits);
console.log(fruitsLength);

// Adding multiple elements to an array
fruitsLength = fruits.push("Avocado", "Guava");
console.log("Mutated array from push() method: ");
console.log(fruits);
console.log(fruitsLength);

// pop()
/**
 * -Removes the last element in an array and returns the removed element
 * Syntax:
 *    arrayName.pop();
 */
console.log("Current Array fruits[]: ");
console.log(fruits);

let fruitRemoved = fruits.pop();
console.log("Mutated array after the pop() method: ");
console.log(fruits);
console.log(fruitRemoved);

fruitRemoved = fruits.pop();
console.log("Mutated array after the pop() method: ");
console.log(fruits);
console.log(fruitRemoved);

// unshift()
/**
 * -Adds one or more elements at the beginning of an array and returns the array's length.
 * -Syntax:
 *    arrayName.unshift('elementA');
 *    arrayName.unshift('elementA', 'elementB')
 */
console.log("Current Array fruits[]: ");
console.log(fruits);

fruitsLength = fruits.unshift("Lime", "Banana");
console.log("Mutated array after the unshift() method: ");
console.log(fruits);
console.log(fruitsLength);

// shift()
/**
 * -Removes the first element in an array and returns the removed element
 * Syntax:
 *    arrayName.shift();
 */
console.log("Current Array fruits[]: ");
console.log(fruits);

fruitRemoved = fruits.shift();
console.log("Mutated array after the shift() method: ");
console.log(fruits);
console.log(fruitRemoved);

// splice()
/**
 * Simultameously removes elements from specified index number and adds elements or remove elements.
 * Syntax:
 *    arrName.splice(startingIndex, deleteCount,            elementsToBeAdded)
 */
console.log("Current Array fruits[]: ");
console.log(fruits);

fruits.splice(1, 1, "Lime");
console.log("Mutated array after the splice() method: ");
console.log(fruits);

fruits.splice(3, 1);
console.log("Mutated array after the splice() method: ");
console.log(fruits);

fruits.splice(3, 0, "Durian", "Santol");
console.log("Mutated array after the splice() method: ");
console.log(fruits);

// sort()
/**
 * IMPORTANT NOTE
 * The sort method is used for more complicated functions Focus the bootcampers on the basic usage of the sort method.
 * -Rearanges the array elements in order.
 * Syntax:
 *    arrayName.sort()
 */
console.log("Current Array fruits[]: ");
console.log(fruits);

fruits.sort();
console.log("Mutated array after the splice() method: ");
console.log(fruits);

// reverse()
/**
 * -reverses the order of array elements
 */
console.log("Current Array fruits[]: ");
console.log(fruits);

fruits.reverse();
console.log("Mutated array after the splice() method: ");
console.log(fruits);

// [Section] Non-mutator Methods
/**
 * - Non-mutator methods are functions that do not modify or change an array after they're created.
 * - These methods do not manipulate the orignl array performing various task such as returning elements from an array and combining arrays and printing the output.
 */

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE", "PH"];

// indexOf()
/**
 * - it returns the index number of the first matching element found in an array
 * - if no match was found, the result will be -1
 * - The search process will be done from first element proceeding to the last element.
 * Syntax: arryName.indexOf(searchValue);
 *          arryName.indexOf(searchValue, startingIndex);
 */
console.log("Current Array countries[]: ");
console.log(countries);
// arryName.indexOf(searchValue);
console.log(countries.indexOf("PH"));
console.log(countries.indexOf("BR"));
// arryName.indexOf(searchValue, startingIndex);
console.log(countries.indexOf("PH", 2));

// lastIndexOf()
/**
 * - returns th eindex number of the last matching element found in an array
 * - the search process will be done from last element proceeding to the first element.
 * Syntax:
 *      arrayName.lastIndexOf(searchValue);
 *      arrayName.lastIndexOf(searchValue, startingIndex);
 */
// arrayName.lastIndexOf(searchValue);
console.log(countries.lastIndexOf("PH"));
// console.log(countries.indexOf("BR"));

// slice()
/**
 * - portion/slices from array and returns a new array
 * Syntax:
 *      arrayName.slice(startingIndex);
 *      arrayName.slice(startingIndex, endingIndex)
 */

// Slicing off elements from a specified index to the last element
console.log(countries);
const slicedArrayA = countries.slice(2);
console.log(slicedArrayA);

const slicedArrayB = countries.slice(1, 5);
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array
const slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);
const slicedArrayD = countries.slice(-5, -1);
console.log(slicedArrayD);

// toString()
/**
 * - returns an array as string seperated by commas
 * Syntax:
 *      arrayName.toString();
 */

let stringedArray = countries.toString();
console.log(stringedArray);

// concat()
/**
 * combines arrays and returns the combined result
 * Syntax:
 *      arrayA.concat(arrayB);
 *      arrayA.concat(elementA);
 */

const tasksArrayA = ["drink HTML", "eat javascript"];
const tasksArrayB = ["inhale CSS", "breathe SASS"];
const tasksArrayC = ["get git", "be node"];

let task = tasksArrayA.concat(tasksArrayB);
console.log("result from concat() method: ");
console.log(task);
// combining multiple arrays
const allTask = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("result from concat() method with multiple array argument: ");
console.log(allTask);

let combinedTask = tasksArrayA.concat("smell express", "throw react");
console.log("result from concat() method: ");
console.log(combinedTask);

// join()
/**
 *
 * - returns an array as string separated by specified separator string
 * Syntax:
 *      arrayName.join('seperatorString)
 */

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));

// [Section] Iteration Methods
/**
 *
 * Iteration methods are loop designed to perform repetitive task on arrays.
 * Iteration method loops over all elements in array.
 */

// forEach()
/**
 * - similar to for loop that iterates on each of array element
 * - for each element in the array, the function in the foreach method will be run.
 * Syntax:
 *      arrayName.forEach(function(indivElement){
 *          statement/s;
 *      })
 */

console.log(allTask);

allTask.forEach(function (task) {
  console.log(task);
});

let filteredTasks = [];
allTask.forEach((task) => {
  if (task.length > 10) filteredTasks.push(task);
});
console.log(filteredTasks);

// map()
/**
 * - iterates on each element and returns new array with different values depending on the result of the function's operation.
 * Syntax:
 *      let/const resulArray = arrayName.map(function(elements){
 *          statements;
 *          return;
 *      })
 */
const numbers = [1, 2, 3, 4, 5];

const numberMap = numbers.map((number) => number * number);
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);

// every()
/**
 * - checks if all elements in an array meet the given condition
 * -this is useful validating data stored in arrays. especially when dealing with large amounts of data.
 * - returns a true value if all elements meet the condition and false if otherwise.
 * Syntax:
 *      let/const resultArray = arrayName.every(function(element){
 *          return expression/condition;
 *       })
 *
 */
console.log(numbers);
let allValid = numbers.every((number) => number < 3);
console.log(allValid);

// some()
/**
 * - checks if at least one element in the array meets the given condition
 * - returns a true value if atleast one element meets the condition and false if none.
 * Syntax:
 *        let/const resultArray = arrayName.some(function(element){
 *            return expression/condition
 *        })
 */
console.log(numbers);
let someValid = numbers.some((number) => number < 2);
console.log(someValid);

// filter()
/**
 * - return new array that contains the elements which meets the given condition
 *  - return an empty array if no element/s were found.
 * Syntax:
 *       let/const resultArray = arrayName.filter(function(element){
 *             return expression/condition;
 *        })
 */
console.log(numbers);
let filteredNumbers = numbers.filter((number) => number < 3);
console.log(filteredNumbers);

// includes()
/**
 * - includes() checks if the argument passed can be found in the array.
 * - it returns boolean which can be save in variable
 * - returns true if the argument is found in the array
 * - returns false if it is not.
 * Syntax:
 *      arrayName.includs(argument)
 */
const products = ["Mouse", "Keyboard", "Laptop", "CPU", "Monitor"];

const productFound1 = products.includes("Mouse");
console.log(productFound1);
const productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce();
/**
 * - evaluate elements from left to right and returns/reduces the array into a single value
 * Syntax:
 *      let/const resultValue = arrayName.reduce(function( accumulator, currentValue ){
 *         return expression.operation
 *      })
 */
console.log(numbers);
let total = numbers.reduce((x, y) => x + y);
console.log(total);
